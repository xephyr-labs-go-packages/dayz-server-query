package main

import (
	"encoding/json"
	"gitlab.com/xephyr-labs-go-packages/dayz-server-query/dzsq"
	"log"
	"time"
)

func main() {
	//client, err := geyser.New(
	//	geyser.WithDebug(),
	//	geyser.WithKey("A116A08AA4DAAEBED4AAEDF8EF03E8A9"),
	//	geyser.WithLanguage("en_US"),
	//)
	//
	//if err != nil {
	//	log.Fatal(err)
	//}
	//
	//gs, err := client.GameServersService()
	//
	////gs, err := geyser.NewGameServersService(client)
	//
	//if err != nil {
	//	log.Fatal(err)
	//}
	//
	//servers, err := gs.GetServerList()
	//
	//if err != nil {
	//	log.Fatal(err)
	//}
	//
	//log.Println(servers.Result)

	s, err := dzsq.NewConnection([]byte("10.72.117.40"), 27016, time.Second*4)

	if err != nil {
		log.Println("[error]: ", err)
	}

	rules, err := s.Info()

	if err != nil {
		log.Println(err)
	}

	b, err := json.MarshalIndent(rules, "", "    ")

	if err != nil {
		log.Println("[error]: ", err)
	}

	log.Println(string(b))
}
