package debug

import (
	"log"
	"os"
)

var Enabled bool
var Logger = log.New(os.Stderr, "[DEBUG] ", 0)

func Printf(format string, args ...any) {
	if Enabled {
		Logger.Printf(format, args...)
	}
}
