package dzsq

import (
	"bytes"
	"encoding/binary"
	"errors"
	"fmt"
	orderedmap "github.com/wk8/go-ordered-map/v2"
	"gitlab.com/xephyr-labs-go-packages/dayz-server-query/debug"
	"math"
	"net"
	"strconv"
	"strings"
	"time"
)

var steamPacketSize = 4096 // Should be 1400, but a2s_rules does not follow this standard with DayZ.

var a2sSinglePacketHeader = []byte{0xFF, 0xFF, 0xFF, 0xFF}

var a2sMultiPacketHeader = []byte{0xFF, 0xFF, 0xFF, 0xFE}

var a2sChallengeHeader = byte(0x41)

type a2sRequest struct {
	Request        []byte
	Start          []byte
	Header         []byte
	Payload        []byte
	Stop           []byte
	Challenge      []byte
	ResponseHeader byte
}

func a2sInfoRequest() a2sRequest {
	stop := []byte{0x00}
	header := []byte{0x54}
	payload := []byte{0x53, 0x6F, 0x75, 0x72, 0x63, 0x65, 0x20, 0x45, 0x6E, 0x67, 0x69, 0x6E, 0x65, 0x20, 0x51, 0x75, 0x65, 0x72, 0x79}
	request := append(a2sSinglePacketHeader, append(append(header, payload...), stop...)...)

	return a2sRequest{
		Request:        request,
		Start:          a2sSinglePacketHeader,
		Header:         header,
		Payload:        payload,
		Stop:           stop,
		Challenge:      request,
		ResponseHeader: 0x49,
	}
}

func a2sPlayerRequest() a2sRequest {
	stop := []byte{0xFF, 0xFF, 0xFF, 0xFF}
	header := []byte{0x55}
	request := append(a2sSinglePacketHeader, append(header, stop...)...)
	challenge := append(a2sSinglePacketHeader, header...)

	return a2sRequest{
		Request:        request,
		Start:          a2sSinglePacketHeader,
		Header:         header,
		Stop:           stop,
		Challenge:      challenge,
		ResponseHeader: 0x44,
	}
}

func a2sRulesRequest() a2sRequest {
	stop := []byte{0xFF, 0xFF, 0xFF, 0xFF}
	header := []byte{0x56}
	request := append(a2sSinglePacketHeader, append(header, stop...)...)
	challenge := append(a2sSinglePacketHeader, header...)

	return a2sRequest{
		Request:        request,
		Start:          a2sSinglePacketHeader,
		Header:         header,
		Stop:           stop,
		Challenge:      challenge,
		ResponseHeader: 0x45,
	}
}

func a2sPingRequest() a2sRequest {
	header := []byte{0x69}
	request := append(a2sSinglePacketHeader, header...)

	return a2sRequest{
		Request:        request,
		Start:          a2sSinglePacketHeader,
		Header:         header,
		ResponseHeader: 0x6A,
	}
}

type dataBuffer []byte

type InfoResponse struct {
	ProtocolVersion uint8    `json:"protocol_version"`
	Name            string   `json:"name"`
	SteamID         uint64   `json:"steam_id"`
	GameID          uint64   `json:"game_id"`
	IP              string   `json:"ip"`
	QueryPort       uint32   `json:"query_port"`
	Ping            float32  `json:"ping"`
	Map             string   `json:"map"`
	Folder          string   `json:"folder"`
	Game            string   `json:"game"`
	ID              int16    `json:"id"`
	Players         uint8    `json:"players"`
	MaxPlayers      uint8    `json:"max_players"`
	Bots            uint8    `json:"bots"`
	ServerType      string   `json:"server_type"`
	Environment     string   `json:"environment"`
	Visibility      string   `json:"visibility"`
	VAC             string   `json:"vac"`
	Version         string   `json:"version"`
	GamePort        int16    `json:"game_port"`
	Keywords        Keywords `json:"keywords"`
}

type Keywords struct {
	Battleye                       bool    `json:"battleye"`
	ThirdPersonDisabled            bool    `json:"third_person_disabled"`
	External                       bool    `json:"external"`
	PrivateHive                    bool    `json:"private_hive"`
	Modded                         bool    `json:"modded"`
	AllowsFilePatching             bool    `json:"allows_file_patching"`
	ShardID                        uint32  `json:"shard_id"`
	LoginQueueSize                 uint32  `json:"login_queue_size"`
	EnvironmentTimeMultiplier      float64 `json:"environment_time_multiplier"`
	EnvironmentNightTimeMultiplier float64 `json:"environment_night_time_multiplier"`
	ServerTime                     string  `json:"server_time"`
}

type PlayersResponse struct {
	Ping    float32  `json:"ping"`
	Players []Player `json:"players"`
}

type Player struct {
	Index    uint8   `json:"index"`
	Name     string  `json:"name"`
	Score    int32   `json:"score"`
	Duration float32 `json:"duration"`
}

type RulesResponse struct {
	IP        string                                 `json:"ip"`
	QueryPort int                                    `json:"query_port"`
	ExtraData bool                                   `json:"extra_data"`
	Ping      float32                                `json:"ping"`
	Rules     *orderedmap.OrderedMap[string, string] `json:"rules"`
	Mods      Mods                                   `json:"mods"`
}

type Mods struct {
	ProtocolVersion uint8      `json:"protocol_version"`
	GeneralFlags    uint8      `json:"general_flags"`
	DLCFlags        uint8      `json:"dlc_flags"`
	ReservedData    uint8      `json:"reserved_data"`
	ModCount        uint8      `json:"mod_count"`
	Mods            []Mod      `json:"mods"`
	Signatures      Signatures `json:"signatures"`
}

type Mod struct {
	Hash    int32  `json:"hash"`
	SteamID uint32 `json:"steam_id"`
	Name    string `json:"name"`
	URL     string `json:"url"`
}

type Signatures []string

type PingResponse struct {
	Ping string `json:"ping"`
}

type Server struct {
	IP         []byte
	Port       int
	Timeout    time.Duration
	connection net.Conn
}

func (s *Server) Close() {
	err := s.connection.Close()
	if err != nil {
	}
}

func NewConnection(ip []byte, port int, timeout time.Duration) (s *Server, err error) {
	s = &Server{
		IP:      ip,
		Port:    port,
		Timeout: timeout,
	}

	addr, err := net.ResolveUDPAddr("udp", string(s.IP)+":"+strconv.Itoa(s.Port))

	if err != nil {
		return nil, err
	}

	conn, err := net.DialUDP("udp", nil, addr)

	if err != nil {
		return nil, err
	}

	s.connection = conn

	err = s.connection.SetReadDeadline(time.Now().Add(s.Timeout))

	if err != nil {
		return nil, err
	}

	return s, nil
}

func (b *dataBuffer) getRaw() (d byte, err error) {
	if 1 > len(*b) {
		return 0, errors.New("index out of range")
	}
	d = (*b)[0]
	*b = (*b)[1:]
	return
}

func (b *dataBuffer) getByte() (d uint8, err error) {
	if 1 > len(*b) {
		return 0, errors.New("index out of range")
	}
	d = (*b)[0]
	*b = (*b)[1:]
	return
}

func (b *dataBuffer) getShort() (d int16, err error) {
	if 2 > len(*b) {
		return 0, errors.New("index out of range")
	}
	d = int16((*b)[0]) | int16((*b)[1])<<8
	*b = (*b)[2:]
	return
}

func (b *dataBuffer) getULong() (n uint32, err error) {
	if 4 > len(*b) {
		return 0, errors.New("index out of range")
	}
	n = binary.LittleEndian.Uint32(*b)
	*b = (*b)[4:]
	return
}

func (b *dataBuffer) getLong() (n int32, err error) {
	if 4 > len(*b) {
		return 0, errors.New("index out of range")
	}
	n = int32((*b)[0]) | int32((*b)[1])<<8 | int32((*b)[2])<<16 | int32((*b)[3])<<24
	*b = (*b)[4:]
	return
}

func (b *dataBuffer) getLongLong() (n uint64, err error) {
	if 8 > len(*b) {
		return 0, errors.New("index out of range")
	}
	n = binary.LittleEndian.Uint64(*b)
	*b = (*b)[8:]
	return
}

func (b *dataBuffer) getFloat() (n float32, err error) {
	if 4 > len(*b) {
		return 0, errors.New("index out of range")
	}
	n = math.Float32frombits(binary.LittleEndian.Uint32(*b))
	*b = (*b)[4:]
	return
}

func (b *dataBuffer) getReadableNullTerminatedString() (d string, err error) {
	shift := 0
	for _, c := range *b {
		shift++
		if c != 0x00 {
			if c >= 32 && c < 127 {
				d += string(c)
			}
		} else {
			break
		}
	}

	if shift == 0 {
		return "", errors.New("index out of range")
	}

	*b = (*b)[shift:]

	return
}

func (b *dataBuffer) getString(c int) (d string, err error) {
	shift := 0
	for i := 0; i < c; i++ {
		if i > len(*b) {
			return "", errors.New("index out of range")
		}
		shift++
		d += string((*b)[i])
	}

	if shift == 0 {
		return "", errors.New("index out of range")
	}

	*b = (*b)[shift:]

	return
}

func (b *dataBuffer) getNullTerminatedBytes() (d []byte, err error) {
	shift := 0
	for _, c := range *b {
		shift++
		if c != 0x00 {
			d = append(d, c)
		} else {
			break
		}
	}

	if shift == 0 {
		return nil, errors.New("index out of range")
	}

	*b = (*b)[shift:]

	return
}

func (b *dataBuffer) getBytes(c int) (d []byte, err error) {
	shift := 0
	for i := 0; i < c; i++ {
		shift++
		d = append(d, (*b)[i])
	}

	if shift == 0 {
		return nil, errors.New("index out of range")
	}

	*b = (*b)[shift:]

	return
}

func decodeTags(ts []string) (k Keywords, err error) {
	for _, t := range ts {
		if t == "battleye" {
			k.Battleye = true
			continue
		}

		if t == "no3rd" {
			k.ThirdPersonDisabled = true
			continue
		}

		if t == "external" {
			k.External = true
			continue
		}

		if t == "privHive" {
			k.PrivateHive = true
			continue
		}

		if t == "mod" {
			k.Modded = true
			continue
		}

		if t == "allowedFilePatching" {
			k.AllowsFilePatching = true
			continue
		}

		if strings.Contains(t, "shard") {
			if len(t[5:]) > 0 {
				i, err := strconv.Atoi(t[5:])

				if err != nil {
					return Keywords{}, errors.New("could not unpack shard tag")
				}

				k.ShardID = uint32(i)
			} else {
				k.ShardID = 0
			}
			continue
		}

		if strings.Contains(t, "lqs") {
			i, err := strconv.Atoi(t[3:])

			if err != nil {
				return Keywords{}, errors.New("could not unpack lqs tag")
			}

			k.LoginQueueSize = uint32(i)
			continue
		}

		if strings.Contains(t, "etm") {
			i, err := strconv.ParseFloat(t[3:], 64)

			if err != nil {
				return Keywords{}, errors.New("could not unpack etm tag")
			}

			k.EnvironmentTimeMultiplier = i
			continue
		}

		if strings.Contains(t, "entm") {
			i, err := strconv.ParseFloat(t[4:], 64)

			if err != nil {
				return Keywords{}, errors.New("could not unpack entm tag")
			}

			k.EnvironmentNightTimeMultiplier = i
			continue
		}

		if strings.Contains(t, ":") && len(t) == 5 {
			k.ServerTime = t
			continue
		}
	}

	return
}

func decodeServerType(s string) (t string) {
	if s == "d" {
		return "dedicated"
	} else if s == "l" {
		return "non-dedicated"
	} else if s == "p" {
		return "proxy"
	} else {
		return s
	}
}

func decodeServerEnvironment(s string) (t string) {
	if s == "l" {
		return "linux"
	} else if s == "w" {
		return "windows"
	} else if s == "m" || s == "o" {
		return "mac"
	} else {
		return s
	}
}

func decodeVAC(i uint8) (t string) {
	if i == 0 {
		return "unsecured"
	} else if i == 1 {
		return "secured"
	} else {
		return strconv.Itoa(int(i))
	}
}

func decodeVisibility(i uint8) (t string) {
	if i == 0 {
		return "public"
	} else if i == 1 {
		return "private"
	} else {
		return strconv.Itoa(int(i))
	}
}

func (b *dataBuffer) handleMultiPacketResponse() (err error) {
	if bytes.Compare(a2sMultiPacketHeader, (*b)[:len(a2sMultiPacketHeader)]) == 0 {
		return errors.New("multi packet response not implemented")
	} else if bytes.Compare(a2sSinglePacketHeader, (*b)[:len(a2sSinglePacketHeader)]) == 0 {
		*b = (*b)[len(a2sSinglePacketHeader):]
	} else {
		return errors.New("unknown data format")
	}

	return
}

func (s *Server) sendPacket(r a2sRequest) (b *dataBuffer, ping float32, err error) {
	buffer := make(dataBuffer, steamPacketSize)

	debug.Printf("[SENDING]: %X", r.Request)

	startTime := time.Now()
	n, err := s.connection.Write(r.Request)
	if err != nil {
		return nil, 0, errors.New(fmt.Sprintf("server did not accept request: %s", err))
	}

	n, err = s.connection.Read(buffer)
	ping = float32(time.Now().Sub(startTime)) / float32(time.Second)

	if buffer == nil || n == 0 {
		return nil, 0, errors.New("[initial] server did not respond")
	}

	err = buffer.handleMultiPacketResponse()

	if buffer == nil || n == 0 {
		return nil, 0, err
	}

	challengeHeader, err := buffer.getRaw()

	if err != nil {
		return nil, 0, err
	}

	if challengeHeader == a2sChallengeHeader {
		challengeResponse := append(r.Challenge, buffer[:4]...)

		_, err = s.connection.Write(challengeResponse)

		if err != nil {
			return nil, 0, errors.New(fmt.Sprintf("server did not accept challenge: %s", err))
		}

		n, err = s.connection.Read(buffer)

		if buffer == nil || n == 0 {
			return nil, 0, errors.New("[challenge] server did not respond")
		}

		buffer = buffer[:n]

		err = buffer.handleMultiPacketResponse()

		if buffer == nil || n == 0 {
			return nil, 0, err
		}
	}

	debug.Printf("[RECEIVING]: %X", buffer)

	responseHeader, err := buffer.getRaw()

	if err != nil {
		return nil, 0, err
	}

	if responseHeader != r.ResponseHeader {
		return nil, 0, errors.New("invalid server response")
	}

	return &buffer, ping, nil
}

func (s *Server) Info() (info InfoResponse, err error) {
	b, ping, err := s.sendPacket(a2sInfoRequest())

	info.Ping = ping

	if err != nil {
		return InfoResponse{}, err
	}

	protocolVersionData, err := b.getByte()

	if err != nil {
		return InfoResponse{}, err
	}

	nameData, err := b.getReadableNullTerminatedString()
	mapData, err := b.getReadableNullTerminatedString()
	folderData, err := b.getReadableNullTerminatedString()
	gameData, err := b.getReadableNullTerminatedString()

	idData, err := b.getShort()
	playersData, err := b.getByte()
	maxPlayersData, err := b.getByte()
	botsData, err := b.getByte()
	serverTypeData, err := b.getByte()
	environmentData, err := b.getByte()
	visibilityData, err := b.getByte()
	vacData, err := b.getByte()
	versionData, err := b.getReadableNullTerminatedString()

	if err != nil {
		return InfoResponse{}, err
	}

	var gamePortData int16
	var keywordsData string
	var steamId uint64
	var gameID uint64

	if len(*b) > 0 {
		extraDataFlag, err := b.getRaw()

		// Game Port
		if extraDataFlag&0x80 != 0x00 {
			gamePortData, err = b.getShort()
		}

		// Steam ID
		if extraDataFlag&0x10 != 0x00 {
			steamId, err = b.getLongLong()
		}

		// SourceTV
		if extraDataFlag&0x40 != 0x00 {
			_, err = b.getShort()
			_, err = b.getReadableNullTerminatedString()
		}

		// Keywords
		if extraDataFlag&0x20 != 0x00 {
			keywordsData, err = b.getReadableNullTerminatedString()
		}

		// GameID
		if extraDataFlag&0x01 != 0x00 {
			gameID, err = b.getLongLong()
		}

		if err != nil {
			return InfoResponse{}, err
		}
	}

	if len(keywordsData) == 0 {
		return InfoResponse{}, errors.New("malformed keywords data")
	}

	keywords, err := decodeTags(strings.Split(keywordsData, ","))

	if err != nil {
		return InfoResponse{}, err
	}

	return InfoResponse{
		ProtocolVersion: protocolVersionData,
		Name:            nameData,
		SteamID:         steamId,
		GameID:          gameID,
		IP:              string(s.IP),
		QueryPort:       uint32(s.Port),
		Map:             mapData,
		Folder:          folderData,
		Game:            gameData,
		ID:              idData,
		Players:         playersData,
		MaxPlayers:      maxPlayersData,
		Bots:            botsData,
		ServerType:      decodeServerType(string(serverTypeData)),
		Environment:     decodeServerEnvironment(string(environmentData)),
		Visibility:      decodeVisibility(visibilityData),
		VAC:             decodeVAC(vacData),
		Version:         versionData,
		GamePort:        gamePortData,
		Keywords:        keywords,
	}, err
}

func (s *Server) Players() (players PlayersResponse, err error) {
	b, ping, err := s.sendPacket(a2sPlayerRequest())

	players.Ping = ping

	if err != nil {
		return PlayersResponse{}, err
	}

	playerCountData, err := b.getByte()

	if err != nil {
		return PlayersResponse{}, err
	}

	for i := 0; i < int(playerCountData); i++ {
		playerIndex, err := b.getByte()
		playerName, err := b.getReadableNullTerminatedString()
		playerScore, err := b.getLong()
		playerDuration, err := b.getFloat()

		if err != nil {
			return PlayersResponse{}, err
		}

		players.Players = append(
			players.Players,
			Player{
				Index:    playerIndex,
				Name:     playerName,
				Score:    playerScore,
				Duration: playerDuration,
			},
		)
	}

	return
}

func (s *Server) Rules() (rules RulesResponse, err error) {
	rulesOrderedMap := orderedmap.New[string, string]()

	b, ping, err := s.sendPacket(a2sRulesRequest())

	rules.Ping = ping

	if err != nil {
		return RulesResponse{}, err
	}

	if err != nil {
		return RulesResponse{}, err
	}

	debug.Printf("[RULES]: %X", *b)

	rulesCountData, err := b.getShort()

	customRuleBytes := make(dataBuffer, 0, steamPacketSize)

	customRulesCount, err := b.getNullTerminatedBytes()

	if err != nil {
		return RulesResponse{}, err
	}

	customRuleChunk, err := b.getNullTerminatedBytes()

	if err != nil {
		return RulesResponse{}, err
	}

	customRuleBytes = append(customRuleBytes, customRuleChunk...)

	for i := 0; i < int(customRulesCount[1]-1); i++ {
		_, err = b.getNullTerminatedBytes()

		if err != nil {
			return RulesResponse{}, err
		}

		customRuleChunk, err := b.getNullTerminatedBytes()

		if err != nil {
			return RulesResponse{}, err
		}

		customRuleBytes = append(customRuleBytes, customRuleChunk...)
	}

	for i := 0; i < int(rulesCountData)-int(customRulesCount[1]); i++ {
		key, err := b.getReadableNullTerminatedString()
		value, err := b.getReadableNullTerminatedString()

		if err != nil {
			return RulesResponse{}, err
		}

		rulesOrderedMap.Set(key, value)
	}

	rules.Rules = rulesOrderedMap

	customRuleBytes = bytes.ReplaceAll(customRuleBytes, []byte{0x01, 0x02}, []byte{0x00})
	customRuleBytes = bytes.ReplaceAll(customRuleBytes, []byte{0x01, 0x03}, []byte{0xFF})
	customRuleBytes = bytes.ReplaceAll(customRuleBytes, []byte{0x01, 0x01}, []byte{0x01})

	debug.Printf("[CUSTOM RULES]: %X", customRuleBytes)

	protocolVersionData, err := customRuleBytes.getByte()
	generalFlagsData, err := customRuleBytes.getByte()
	dlcFlagsData, err := customRuleBytes.getByte()
	reservedData, err := customRuleBytes.getByte()

	if err != nil {
		return rules, nil
	}

	island, present := rules.Rules.Get("island")

	// If there is no island we cannot determine edge cases.
	if !present {
		return RulesResponse{}, errors.New("island not defined")
	}

	// Edge case where BI provided DLC has a hash for the island.
	if island == "enoch" || island == "Enoch" || island == "ENOCH" {
		_, err = customRuleBytes.getLong()

		if err != nil {
			return rules, nil
		}
	}

	numberOfModsData, err := customRuleBytes.getByte()

	if err != nil {
		return rules, nil
	}

	rules.Mods = Mods{
		ProtocolVersion: protocolVersionData,
		GeneralFlags:    generalFlagsData,
		DLCFlags:        dlcFlagsData,
		ReservedData:    reservedData,
		ModCount:        numberOfModsData,
	}

	outOfRange := false

	for i := 0; i < int(numberOfModsData); i++ {
		modHash, err := customRuleBytes.getLong()
		modIDLength, err := customRuleBytes.getByte()

		var modID uint32

		// Edge case where sometimes the modIDLength is not present and data seams to be corrupted. version < v1.19
		if modIDLength != 0x04 {
			//_ = customRuleBytes.getByte()
			outOfRange = true
			break
		} else {
			modID, err = customRuleBytes.getULong()
		}

		modNameLength, err := customRuleBytes.getByte()
		modName, err := customRuleBytes.getString(int(modNameLength))

		if err != nil {
			outOfRange = true
			break
		}

		mod := Mod{
			Hash:    modHash,
			SteamID: modID,
			Name:    modName,
		}

		if modID != 0 {
			mod.URL = fmt.Sprint("https://steamcommunity.com/sharedfiles/filedetails/?id=", modID)
		}

		rules.Mods.Mods = append(
			rules.Mods.Mods,
			mod,
		)
	}

	if !outOfRange {
		signaturesCount, _ := customRuleBytes.getByte()

		for i := 0; i < int(signaturesCount); i++ {
			signatureLength, err := customRuleBytes.getByte()
			signature, err := customRuleBytes.getString(int(signatureLength))

			if err != nil {
				break
			}

			rules.Mods.Signatures = append(rules.Mods.Signatures, signature)
		}
	}

	rules.ExtraData = len(customRuleBytes) != 0

	rules.IP = string(s.IP)
	rules.QueryPort = s.Port

	return
}

func (s *Server) Ping() (ping PingResponse, err error) {
	b, _, err := s.sendPacket(a2sPingRequest())

	if err != nil {
		return PingResponse{}, err
	}

	ping.Ping, err = b.getReadableNullTerminatedString()

	if err != nil {
		return PingResponse{}, err
	}

	return
}
